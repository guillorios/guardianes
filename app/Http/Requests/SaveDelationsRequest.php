<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveDelationsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'acto' => 'required',
            'happen' => 'required',
            'job' => 'required',
            'evidence' => 'required|mimes:jpg,jpeg,gif,png,xls,xlsx,doc,docx,pdf|max:5000',
        ];

        return $rules;
    }


    public function messages()
    {
        return [
            'acto.required' => "Debe describir su denuncia.",
            'happen.required' => "Debe escribir una ubicación.",
            'job.required' => "Debe escribir el cargo o funcionario implicado.",
            'evidence.required' => "Debe escoger una imagen o documento.",
            'evidence.max' => "Debe escoger una imagen o documento de menor tamaño.",
            'evidence.mimes' => "El formato del archivo no es válido.",
        ];
    }
}
