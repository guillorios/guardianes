<?php

namespace App\Widgets;

use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;
use Illuminate\Support\Facades\Auth;

class Delation extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count =\App\Delation::count();
        $string = trans_choice(__("Denuncia|Denuncias"), $count);

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-skull',
            'title'  => "{$count} {$string}",
            'text'   => __('Tiene :count :string en su base de datos. Haga clic en el boton de abajo para ver todas las Denuncias.', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('Ver todas las denuncias'),
                'link' => route('voyager.delations.index'),
            ],
            'image' => 'images/widget-app.png',
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return app('VoyagerAuth')->user()->can('browse', new \App\Delation);
    }
}
