<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Guardianes | Inicio</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
        <script src="https://code.jquery.com/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>


    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col img">
                    <a title="" href="{{url('/')}}"><img class="img-fluid" src="images/logoguardianes.png" width="200" height="200" alt="Denuncia" /></a>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <img class="img-fluid" src="images/registro_exitoso.png" width="800" height="400" alt="Denuncia" />
                </div>
            </div>
            <div class="row">
                <div class="col img">
                    <a title="" href="{{url('/formulario')}}"><img src="images/otradenuncia.png" class="img-fluid" width="470" height="500" alt="Denuncia" /></a>
                </div>
            </div>
          </div>
    </body>
</html>
