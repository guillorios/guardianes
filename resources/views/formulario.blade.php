<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />

     <!-- Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

     <script src="https://code.jquery.com/jquery.min.js"></script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>


    <title>Guardianes | Formulario</title>

</head>
<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col img">
                <a title="" href="{{url('/')}}"><img class="img-fluid" src="images/logoguardianes.png" width="200" height="200" alt="Denuncia" /></a>
            </div>
        </div>
        <div class="row justify-content-center">

            <div class="col-12 col-md-10 col-lg-8 col-xl-6 mb-3 portada">

                <img src="images/letrero_formulario_blanco.png" class="img-fluid" width="400" height="90" alt="Enviar" />

                <form action="{{ url('/delation') }}" method="POST" enctype="multipart/form-data">

                    @csrf

                    <div class="form-group">
                        <label class="control-label label-form float-left" for="formGroupExampleInput">Describa el acto de corrupción</label>
                        <textarea rows="10" class="form-control" name="acto" id ="acto" type="text" placeholder="Escriba aquí ...">{{ old('acto') }}</textarea>
                        {!! $errors->first('acto', '<small class="bg-danger text-white">:message</small>') !!}
                    </div>
                    <div class="form-group">
                        <label class="control-label label-form float-left" for="formGroupExampleInput2">¿ Dónde ocurre ?</label>
                        <input type="text" class="form-control" name="happen" id="happen" value = "{{ old('happen') }}" placeholder="Escriba aquí ...">
                        {!! $errors->first('happen', '<small class="bg-danger text-white">:message</small>') !!}
                    </div>
                    <div class="form-group">
                        <label class="control-label label-form float-left" for="formGroupExampleInput2">Cárgo o funcionario implicado</label>
                        <input type="text" class="form-control" name="job" id="job" value = "{{ old('job') }}" placeholder="Escriba aquí ...">
                        {!! $errors->first('job', '<small class="bg-danger text-white">:message</small>') !!}
                    </div>
                    <div class="form-group">
                        <label class="control-label label-form float-left">Anexa una prueba (imágen o documento máx 5 mb, jpg, jpeg, gif, png, xls, xlsx, doc, docx, pdf)</label>
                        <input type="file" class="form-control" name="evidence" id="evidence">
                        {!! $errors->first('evidence', '<small class="bg-danger text-white">:message</small>') !!}
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12 col-sm-9 col-md-4">
                            <button class="btn btn-primary btn-block" name="send" id="boton">ENVIAR</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
