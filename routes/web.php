<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/delation', 'DelationController@store');

Route::get('/formulario', function () {
    return view('formulario');
});

Route::get('/registro', function () {
    return view('registro');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
